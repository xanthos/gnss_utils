///
/// @file rnxnav.hpp
///
#ifndef __NAVIGATION_RINEX_HPP__
#define __NAVIGATION_RINEX_HPP__

#include <iostream>
#include <fstream>
#include "satsys.hpp"
#include "ggdatetime/gnsstm.hpp"
#include "ggdatetime/dtcalendar.hpp"

namespace ngpt
{

/// @class Nav_TimeSysCor
/// A class to hold a time-system correction, as specified in RINEX v3.03.
class Nav_TimeSysCor
{
public:

private:
    ngpt::GnssTimeSystem __from,
                         __to;
    double               __a0, ///< Coefficients of 1deg polynomial in sec
                         __a1; ///< Coefficients of 1deg polynomial in sec/sec
};// class Nav_TimeSysCor

/// Leading whitespace characters are skipped (if any).
Nav_TimeSysCor(const char* line)
{
    int ofst = 0;
    char* end;
    while (line[ofst] == ' ') ++ofst;
    
    if ( !std::strncmp(line+ofst, "GAUT", 4) ) {
        __from = ngpt::GnssTimeSystem::gal;
        __to   ngpt::GnssTimeSystem::utc;
    } else if ( !std::strncmp(line+ofst, "GPUT", 4) ) { 
        __from = ngpt::GnssTimeSystem::gps;
        __to   ngpt::GnssTimeSystem::utc;
    } else if ( !std::strncmp(line+ofst, "SBUT", 4) ) { 
        __from = ngpt::GnssTimeSystem::sbas;
        __to   ngpt::GnssTimeSystem::utc;
    } else if ( !std::strncmp(line+ofst, "GLUT", 4) ) {
        __from = ngpt::GnssTimeSystem::glo;
        __to   ngpt::GnssTimeSystem::utc;
    } else if ( !std::strncmp(line+ofst, "GPGA", 4) ) {
        __from = ngpt::GnssTimeSystem::gps;
        __to   ngpt::GnssTimeSystem::gal;
    } else if ( !std::strncmp(line+ofst, "GLGP", 4) ) {
        __from = ngpt::GnssTimeSystem::glo;
        __to   ngpt::GnssTimeSystem::gps;
    } else if ( !std::strncmp(line+ofst, "QZGP", 4) ) {
        __from = ngpt::GnssTimeSystem::qzs;
        __to   ngpt::GnssTimeSystem::gps;
    } else if ( !std::strncmp(line+ofst, "QZUT", 4) ) {
        __from = ngpt::GnssTimeSystem::qzs;
        __to   ngpt::GnssTimeSystem::utc;
    } else if ( !std::strncmp(line+ofst, "BDUT", 4) ) {
        __from = ngpt::GnssTimeSystem::bds;
        __to   ngpt::GnssTimeSystem::utc;
    } else if ( !std::strncmp(line+ofst, "IRUT", 4) ) {
        __from = ngpt::GnssTimeSystem::irn;
        __to   ngpt::GnssTimeSystem::utc;
    } else if ( !std::strncmp(line+ofst, "IRGP", 4) ) {
        __from = ngpt::GnssTimeSystem::irn;
        __to   ngpt::GnssTimeSystem::gps;
    } else {
        throw std::runtime_error("[ERROR] Nav_TimeSysCor() : Invalid (string) identifier");
    }
    
    ofst += 5;
    __a0 = std::strtod(line+ofst, &end); ofst += 17;
    __a1 = std::strtod(line+ofst, &end); ofst += 16;
    __secofweek = std::strtol(line+ofst, &end, 10); ofst += 7;
    __week = std::strtol(line+ofst, &end, 10);

}

class NavRinex
{
public:
    /// Constructor from filename.
    NavRinex(const char* c)
    : __filename(c),
      __istream(c, std::ios_base::in)
    {
        read_header();
    }

    /// Destructor (closing the file is not mandatory, but nevertheless)
    ~NavRinex() noexcept
    {
        if (__istream.is_open()) __istream.close();
    }

    /// Copy not allowed !
    NavRinex(const NavRinex&) = delete;

    /// Assignment not allowed !
    NavRinex& operator=(const NavRinex&) = delete;

    /// Move Constructor.
    /// @note In gcc 4.8 this only compiles if the noexcept specification is
    ///       commented out
    NavRinex(NavRinex&& a)
        noexcept(std::is_nothrow_move_constructible<std::ifstream>::value)
        = default;

    /// Move assignment operator.
    /// @note In gcc 4.8 this only compiles if the noexcept specification is
    ///       commented out
    NavRinex& operator=(NavRinex&& a)
        noexcept(std::is_nothrow_move_assignable<std::ifstream>::value)
        = default;
    
    /// Read and return the version number
    float
    read_version(SatSys& s);

private:
    /// Let's not write this more than once.
    typedef std::ifstream::pos_type pos_type;

    /// Read header and assign member variables
    int
    read_header();
    
    std::string            __filename; ///< The name of the RINEX file.
    std::ifstream          __istream;  ///< The infput (file) stream.
    ngpt::SatSys           __satsys;   ///< satellite system.
    float                  __version;  ///< The RINEX version
    ngpt::datetime<ngpt::seconds> __start_date;
    double                 __mTauC;
    int                    __leap_sec;
    pos_type               __eoh;      ///< Mark the 'END OF HEADER' field.
}; // NavRinex

class GloNavRinex
{
}; // GloNavRinex

}// namespace ngpt
#endif
