#include "dtfund.hpp"

/* Precise algorithm for determination of position and velocity vector
 * components for the SV’s center of mass for the given instant of MT.
 * A user shall recalculate the ephemerides at the instant t_b in MT into
 * those at the given instant t_i in MT (|τ_i| = |t_i - t_b| < 15min) 
 * using numerical integration of differential equations for the motion of a
 * SV’s center of mass.
 * @cite http://russianspacesystems.ru/wp-content/uploads/2016/08/ICD-GLONASS-CDMA-General.-Edition-1.0-2016.pdf
 *       
 */
void
(double gmst,
double te,
        x0,  y0,  z0,
       vx0, vy0, vz0,
       ax0, ay0, az0)
{
    // rad/s mean angular velocity of the earth
    constexpr double Omega {7.2921151467e-5};

    // sidereal time in the Greenwich meridian at epoch t_e
    double s = gmst + Omega*(te-10800e0);
    // trigonometric numbers
    double cosS = std::cos(s);
    double sinS = std::sin(s);
    
    // Coordinate transformation to an inertial reference frame (ref. GLONASS
    // ICD, J4) at time t_e
    double state[9];   // state vector including acceleration
    state[0] = x0*cosS - y0*sinS;
    state[1] = x0*sinS + y0*cosS;
    state[2] = z0;
    state[3] = vx0*cosS - vy0*sinS - Omega*ya;
    state[4] = vx0*sinS + vy0*cosS + Omega*xa;
    state[5] = vz0;
    state[6] = ax0; // will not change
    state[7] = ay0;
    state[8] = az0;

    // vector of first an second derivatives for Runge-Kutta, aka this will
    // contain the values [x', y', z', x'', y'', z'']
    double rk[4*6];
    double* k1=rk,
           *k2=rk+6,
           *k3=rk+2*6,
           *k4=rk+3*6;
    // gravitational perturbations (sun and moon)
    double jsm[6];
    // current state vector for use in runge-kutta
    double w[6];
    t = te;
    for (it=0;.....) {
        s = s0 + omega*(te-10800e0);
        // Luni-solar perturbative term at time t (in Abs Ref Syst) ....
        cosS = std::cos(s);
        sinS = std::sin(s);
        jsm[0] = state[6]*cosS - state[7]*sinS;
        jsm[1] = state[6]*sinS + state[7]*cosS;
        jsm[2] = state[8];
        // Runge-Kutta 4th order
        // Note that:
        // Y'(t) = F(t, Y(t)), where
        // Y'(t) = [x', y', z', x'', y'', z''],
        // Y(t)  = [x, y, z, x', y', z']
        // and F = [x', y', z', -μx + ...., -yμ+..., -μz+...] as in Eq. (J1)
        glo_orb_deriv(state, jsm, k1);
        for (int i = 0; i < 6; i++) w[i] = state[i] + h*k1[i]/2e0; // aka Y_n+h*K1/2
        glo_orb_deriv(w, jsm, k2);
        for (int i = 0; i < 6; i++) w[i] = state[i] + h*k2[i]/2e0;
        glo_orb_deriv(w, jsm, k3);
        for (int i = 0; i < 6; i++) w[i] = state[i] + h*k3[i];
        glo_orb_deriv(w, jsm, k4);
        for (int i = 0; i < 6; i++)
            state[i] += (k1[i]+2e0*k2[i]+2e0*k3[i]+k4[i])*h/6e0;

        return;
}

/* 
 * @param[in] state  A six-element double array, containing the SV state
 *                   vector, aka [x0, y0, z0, Vx0, Vy0, Vz0] in km and km/sec.
 * @param[in] jsm    A three-element array, containing the accelerations 
 *                   induced by gravitational perturbations of the Sun and
 *                   Moon, aka:
 *                   * j_x0s + j_x0m
 *                   * j_y0s + j_y0m
 *                   * j_z0s + j_z0m
 * @param[out] xdot  A six-element double array, containing the values of the
 *                   derivatives of the state vector (aka 
 *                   [x', y', z', Vx', Vy', Vz']), at the same point. Units are
 *                   km/sec and km/sec2.
 * @cite gloicd16 , Equations J.1
 */
void
glo_orb_deriv(const double* state, const double* jsm, double* xdot)
{
    constexpr double GM = 398600441.8e6;//  geocentric grvitational constant
    constexpr double J2 = 1082625.75e-9;// second degree coefficient of normal potential
    constexpr double ae = 6378136e0;    // meters; semi-major axis of PZ-90
    double r = std::sqrt(state[0]*state[0]+state[2]*state[2]+state[2]*state[2]);
    double xmu = GM/r/r;
    double rho = a/r;
    double xx0 = state[0] / r;
    double xx1 = state[1] / r;
    double xx2 = state[2] / r;
    double cm  = -J2*xmu*3.30/2.30*rho*rho*(1.30-5.e0*xx3*xx3);
    double cmz = -J2*xmu*3.30/2.30*rho*rho*(3.30-5.e0*xx3*xx3);
    xdot[0] = state[3];
    xdot[1] = state[4];
    xdot[2] = state[5];
    xdot[4] = (cm-xmu)*xx0+jsm[0];
    xdot[5] = (cm-xmu)*xx1+jsm[1];
    xdot[6] = (cmz-xmu)*xx2+jsm[2];

    return;
}

/** @brief Compute GMST (in radians)
 *  Compute the sidereal time in the Greenwich meridian at a given date. Note
 *  that time of day is assumed to be 0, aka we assume the begining of the day.
 *  The algorithm follows the formula in Appendix K, "Algorithm for calculation
 *  of the current Julian date JD0, Gregorian date, and GMST" from @cite gloicd16
 *
 *  @param[in] year  The year
 *  @param[in] month The month
 *  @param[in] day   The day of month
 *  @return          GMST in radians for the given date.
 */
double
gmst(int year, int month, int day)
{
    long mjd = ngpt::cal2mjd(year, month, day);
    double targ = static_cast<double>(mjd) - ngpt::j2000_mjd;
    // Earth’s rotation angle in radians
    double era = ngpt::D2PI*(0.779057273264e0+1.00273781191135448e0*targ);
    // time from Epoch 2000, 1st January, 00:00 (UTC(SU)) till the current 
    // epoch in Julian centuries
    double Td = targ/36525.0e0;
    // Compute gmst
    double gmst = (era + 0.0000000703270726e0) +
                        (0.0223603658710194e0 +
                        (0.0000067465784654e0 -
                        (0.0000000000021332e0 +
                        (0.0000000001452308e0 +
                         0.0000000000001784e0*Td)*Td)*Td)*Td)*Td;
    // return
    return gmst;
}
