///
/// @file satsys.cpp
/// 

#include <stdexcept>
#include "satsys_traits.hpp"
#include "satsys.hpp"

/// @details  Given a char, this function will return the corresponding
///           satellite system. The identifier are taken from @cite rnx303
///
/// @param[in] c Input char.
/// @return      The corresponding satellite system as ngpt::SatSys instance
/// @throw       std::runtime_error if no matching satellite system is found.
ngpt::SatSys
ngpt::char2satsys(char c)
{
    using ngpt::SatSys;

    switch ( c )
    {
        case (ngpt::satellite_system_traits<SatSys::gps>::identifier):
            return SatSys::gps;
        case (ngpt::satellite_system_traits<SatSys::glonass>::identifier):
            return SatSys::glonass;
        case (ngpt::satellite_system_traits<SatSys::galileo>::identifier):
            return SatSys::galileo;
        case (ngpt::satellite_system_traits<SatSys::sbas>::identifier):
            return SatSys::sbas;
        case (ngpt::satellite_system_traits<SatSys::qzss>::identifier):
            return SatSys::qzss;
        case (ngpt::satellite_system_traits<SatSys::beidou>::identifier):
            return SatSys::beidou;
        case (ngpt::satellite_system_traits<SatSys::irnss>::identifier):
            return SatSys::irnss;
        case (ngpt::satellite_system_traits<SatSys::mixed>::identifier):
            return SatSys::mixed;
        default :
            throw std::runtime_error
                ("char2satsys -> Invalid Satellite System Identifier!!");
    }
}

/// @details  Given a satellite system enumerator (aka ngpt::SatSys), this function
///           will return it's identifier character (e.g. given 
///           SatSys = GPS, the function will return 'G'). The identifiers are
///           taken from @cite rnx303
///
/// @param[in] s Input satellite system (as ngpt::SatSys)
/// @return      A char, representing the satellite system
/// @throw       std::runtime_error if no matching satellite system is found.
char
ngpt::satsys2char(ngpt::SatSys s)
{
    using ngpt::SatSys;

    switch ( s )
    {
        case SatSys::gps :
            return ngpt::satellite_system_traits<SatSys::gps>::identifier;
        case SatSys::glonass :
            return ngpt::satellite_system_traits<SatSys::glonass>::identifier;
        case SatSys::sbas :
            return ngpt::satellite_system_traits<SatSys::sbas>::identifier;
        case SatSys::galileo :
            return ngpt::satellite_system_traits<SatSys::galileo>::identifier;
        case SatSys::beidou :
            return ngpt::satellite_system_traits<SatSys::beidou>::identifier;
        case SatSys::qzss :
            return ngpt::satellite_system_traits<SatSys::qzss>::identifier;
        case SatSys::irnss :
            return ngpt::satellite_system_traits<SatSys::irnss>::identifier;
        case SatSys::mixed :
            return ngpt::satellite_system_traits<SatSys::mixed>::identifier;
        default:
            throw std::runtime_error
                ("ngpt::satsys2char() -> Invalid Satellite System !!");
    }
}

/// Given a band/frequency number of the calling SatelliteSystem, return the
/// frequency of the carrier in MHz. The values are extracted from @cite rnx303.
///
/// @param[in] band The band for which we want the frequency value. For
///                 band information per satellite-system, see @cite rnx303.
/// @return The nominal frequency of the band, for this SatelliteSystem; note
///         that this may not always be the actual frequency of the carrier
///         (for ngpt::SatSys::glonass we need to know the SVN's slot number
///         to compute the carrier frequency).
/// @throw std::runtime_error if the frequency_map static attribute
///        for the satellite system does not hold this band (see e.g.
///        ngpt::satellite_system_traits<SatSys::gps>::frequency_map)
double
ngpt::SatelliteSystem::band2freq(int band) const
{
    using ngpt::SatSys;

    try {
        switch (__sys) {
            case SatSys::gps:
                return ngpt::satellite_system_traits<SatSys::gps>::frequency_map.at(band);
            case SatSys::glonass:
                return ngpt::satellite_system_traits<SatSys::glonass>::frequency_map.at(band);
            case SatSys::sbas:
                return ngpt::satellite_system_traits<SatSys::sbas>::frequency_map.at(band);
            case SatSys::galileo:
                return ngpt::satellite_system_traits<SatSys::galileo>::frequency_map.at(band);
            case SatSys::beidou:
                return ngpt::satellite_system_traits<SatSys::beidou>::frequency_map.at(band);
            case SatSys::qzss:
                return ngpt::satellite_system_traits<SatSys::qzss>::frequency_map.at(band);
            case SatSys::irnss:
                return ngpt::satellite_system_traits<SatSys::irnss>::frequency_map.at(band);
            default:
                // std::map.at() will throw an std::out_of_range; let's do the
                // same so we won;t have to catch more than one exception types.
                throw std::out_of_range("");
        }
    } catch (std::out_of_range& e) {
        throw std::runtime_error("SatelliteSystem::band2freq()");
    }
}
