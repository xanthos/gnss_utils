#include <string>
#include <cstring>
#include <algorithm>
#include "antenna.hpp"

using ngpt::Antenna;
using ngpt::antenna_details::antenna_model_max_chars;
using ngpt::antenna_details::antenna_radome_max_chars;
using ngpt::antenna_details::antenna_serial_max_chars;
using ngpt::antenna_details::antenna_full_max_chars;

/// The 'NONE' radome type as a c-string.
constexpr char none_radome[] = "NONE";

Antenna::Antenna() noexcept
{
    this->fill_with(' ');
}

Antenna::Antenna(const char* c) noexcept
{
    this->copy_from_cstr(c);
}

Antenna::Antenna(const std::string& s) noexcept
{
    this->copy_from_str(s);
}

inline void
Antenna::fill_with(char c)
noexcept
{
    std::memset(__name, c, antenna_full_max_chars * sizeof(char));
    __name[antenna_full_max_chars-1] = '\0';
}

inline void
Antenna::set_none_radome()
noexcept
{
    std::memcpy(__name+antenna_model_max_chars+1, none_radome, 
                antenna_radome_max_chars * sizeof(char));
}

/// @details      Copy a given std::string to the __name variable. The function
///               will copy MIN(str.size(), antenna_full_max_chars-1)
///               characters from the input string. The input string is not
///               checked, but should follow the naming conventions defined
///               in @cite rcvr_ant. The last character of the __name variable
///               will not be changed.
///
/// @param[in] s  A string representing a valid antenna model name (optionaly
///               including a serial number).
///
/// @note         At most antenna_full_max_chars - 1 characters will be copied;
///               any remaining characters will be ignored.
void
Antenna::copy_from_str(const std::string& s)
noexcept
{
    // set antenna model+radome+serial to ' '
    this->fill_with(' ');
  
    // size of input string
    std::size_t str_size { s.size() };
    std::size_t nrc      { std::min(str_size, antenna_full_max_chars-1) };
    std::copy(s.begin(), s.begin()+nrc, &__name[0]);
}

/// @details      Copy a given c-string to the __name variable. The function
///               will copy MIN(str.size(), antenna_full_max_chars-1)
///               characters from the input string. The input string is not
///               checked, but should follow the naming conventions defined
///               in @cite rcvr_ant. The last character of the __name variable
///               will not be changed.
///
/// @param[in] c A c-string representing a valid antenna model name (optionaly
///              including a serial number). Note that a valid c-string ends
///              with '\0'.
///
/// @note        At most antenna_full_max_chars - 1 characters will be copied;
///              any remaining characters will be ignored.
void
Antenna::copy_from_cstr(const char* c)
noexcept
{
    // set name_ to null.
    this->fill_with(' ');

    // get the size of the input string.
    std::size_t ant_size { std::strlen(c) };

    // copy at maximum antenna_full_max_chars - 1 characters
    std::memcpy(__name, c, sizeof(char) * 
                          std::min(ant_size, antenna_full_max_chars-1) );
}
