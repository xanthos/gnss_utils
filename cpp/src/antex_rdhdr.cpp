#include <iostream>
#include <cstring>
#include <cassert>
#include "antex.hpp"

/// According to the ANTEX definition, the antenna serial number is 20 chars.
constexpr std::size_t ANTEX_SERIAL_CHARS { 20 };

static_assert(ANTEX_SERIAL_CHARS == ngpt::antenna_details::antenna_serial_max_chars,
            "Size of serial number  different in antex.cpp and antenna.hpp");

/// No header line can have more than 80 chars. However, there are cases when
/// they  exceed this limit, just a bit ...
constexpr int MAX_HEADER_CHARS { 85 };

/// Max header lines.
constexpr int MAX_HEADER_LINES { 1000 };

/// Size of 'END OF HEADER' C-string.
/// std::strlen is not 'constexr' so eoh_size can't be one either. Note however
/// that gcc has a builtin constexpr strlen function (if we want to disable this
/// we can do so with -fno-builtin).
#ifdef __clang__
    const     std::size_t eoh_size { std::strlen("END OF HEADER") };
#else
    constexpr std::size_t eoh_size { std::strlen("END OF HEADER") };
#endif

/*
 *  @details Read an Antex (instance) header. The format of the header should
 *           closely follow the antex format specification (version 1.4).
 *           This function will set the instance fields:
 *           - __satsys,
 *           - __eoh
 *           The function will exit after reading a header line, ending with
 *           the (sub)string 'END OF HEADER'.
 *
 *  @throw   std::runtime_error in case ngpt::char2satsys() throws.
 *
 *  @warning
 *           - The instance's input steam (i.e. __istream) should be open and
 *           valid.
 *           - Note that the function expects that no header line contains more
 *           than MAX_HEADER_CHARS chars.
 *           - If the header is not read correctly, then the (opened) file
 *           buffer will be closed.
 *
 *  @return  Anything other than '0' is an error.
 *
 *  @cite atx14
 */
int
ngpt::Antex::read_header()
{
    char line[MAX_HEADER_CHARS];

    // The stream should be open by now!
    if (!(this->__istream.is_open())) {
        std::cerr<<"\n[ERROR] Antex::read_header(): Stream is closed!";
        return 10;
    }

    // Go to the top of the file.
    __istream.seekg(0);

    // Read the first line. Get version and system.
    // ----------------------------------------------------
    __istream.getline(line, MAX_HEADER_CHARS);
    // strtod will keep on reading untill a non-valid
    // char is read. Fuck this, lets split the string
    // so that it only reads the valid chars (for version).
    *(line+15) = '\0';
    float fvers = std::strtod(line,nullptr);
    if (std::abs(fvers - 1.4) > .001) {
        std::cerr<<"\n[ERROR] Antex::read_header(): Invalid Antex version (i.e. not 1.4)";
        return 1;
    }
    // Resolve the satellite system.
    this->__satsys = ngpt::char2satsys(line[20]);

    // Read the second line. Get PCV TYPE / REFANT.
    // ----------------------------------------------------
    __istream.getline(line, MAX_HEADER_CHARS);
    if (*line != 'A') {
        std::cerr<<"\n[ERROR] Antex::read_header(): Relative Antex cannot be resolved.";
        return 2;
    } 
    
    // Keep on readling lines until 'END OF HEADER'.
    // ----------------------------------------------------
    int dummy_it = 0;
    __istream.getline(line, MAX_HEADER_CHARS);
    while (dummy_it < MAX_HEADER_LINES &&
            strncmp(line+60, "END OF HEADER", eoh_size) ) {
        __istream.getline(line, MAX_HEADER_CHARS);
        dummy_it++;
    }
    if (dummy_it >= MAX_HEADER_LINES) {
        std::cerr<<"\n[DEBUG] Antex::read_header() -> Could not find 'END OF HEADER'.";
        return 3;
    }

    this->__eoh = __istream.tellg();

    // All done !
    return 0;
}
