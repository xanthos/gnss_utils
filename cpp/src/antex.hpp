///
/// @file antex.hpp
///
#ifndef __ANTEX_HPP__
#define __ANTEX_HPP__

#include <fstream>
#include "satsys.hpp"
#include "antenna.hpp"

namespace ngpt
{

class Antex
{
public:
    /// Constructor from filename.
    Antex(const char*);

    /// Destructor (closing the file is not mandatory, but nevertheless)
    ~Antex() noexcept
    {
        if (__istream.is_open()) __istream.close();
    }

    /// Copy not allowed !
    Antex(const Antex&) = delete;

    /// Assignment not allowed !
    Antex& operator=(const Antex&) = delete;

    /// Move Constructor.
    /// TODO In gcc 4.8 this only compiles if the noexcept specification is
    ///      commented out
    Antex(Antex&& a)
        /*noexcept(std::is_nothrow_move_constructible<std::ifstream>::value)*/ = default;

    /// Move assignment operator.
    Antex& operator=(Antex&& a)
        /*noexcept(std::is_nothrow_move_assignable<std::ifstream>::value)*/ = default;
private:
    /// Let's not write this more than once.
    typedef std::ifstream::pos_type pos_type;

    /// Read header and assign member variables
    int
    read_header();
    
    std::string            __filename; ///< The name of the antex file.
    std::ifstream          __istream;  ///< The infput (file) stream.
    ngpt::SatSys           __satsys;   ///< satellite system.
    pos_type               __eoh;      ///< Mark the 'END OF HEADER' field.
};// class Antex

}// namespace ngpt
#endif
