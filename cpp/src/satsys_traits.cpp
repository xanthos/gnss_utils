///
/// @file satsys_traits.cpp
///

#include "satsys_traits.hpp"

/// Initialize the static frequency map for SatSys::gps. Values are
/// {band, frequency in MHz}.
const std::map<int, double>
ngpt::satellite_system_traits<ngpt::SatSys::gps>::frequency_map =
{
    { 1, 1575.42e0 },
    { 2, 1227.60e0 },
    { 5, 1176.45e0 }
};

/// Initialize the valid-attribute-list Vs band map for SatSys::gps.
const std::map<int, std::string>
ngpt::satellite_system_traits<ngpt::SatSys::gps>::valid_attributes =
{
    { 1, std::string("CSLXPWYMN " ) },
    { 2, std::string("CDSLXPWYMN ") },
    { 5, std::string("IQX "       ) }
};

/// Initialize the static frequency map for SatSys::glonass. Values are
/// {band, frequency in MHz}.
/// @warning For glonass, the carrier frequency is not the nominal; each SVN
///          transmits on a unique frequency, which must be computed.
const std::map<int, double>
ngpt::satellite_system_traits<ngpt::SatSys::glonass>::frequency_map =
{
    { 1, 1602.000e0 },
    { 2, 1246.000e0 },
    { 3, 1202.025e0 }
};

/// Initialize the valid-attribute-list Vs band map for SatSys::glonass.
/// @note Glonass L3 band is only recordable in RINEX v3.00 and onwards. Hence,
///       such an observable must have an attribute (i.e. ' ' is not a valid
///       attribute for Glonass band-3).
const std::map<int, std::string>
ngpt::satellite_system_traits<ngpt::SatSys::glonass>::valid_attributes =
{
    { 1, std::string("CP " ) },
    { 2, std::string("CP " ) },
    { 3, std::string("IQX" ) }
};

/// Initialize the static frequency map for SatSys::galileo. Values are
/// {band, frequency in MHz}.
const std::map<int, double>
ngpt::satellite_system_traits<ngpt::SatSys::galileo>::frequency_map =
{
    { 1, 1575.420e0 }, ///< E1
    { 5, 1176.450e0 }, ///< E5a
    { 7, 1207.140e0 }, ///< E5b
    { 8, 1191.795e0 }, ///< E5(E5a+E5b)
    { 6, 1278.750e0 }  ///< E6
};

/// Initialize the valid-attribute-list Vs band map for SatSys::galileo.
const std::map<int, std::string>
ngpt::satellite_system_traits<ngpt::SatSys::galileo>::valid_attributes =
{
    { 1, std::string("ABCXZ ") },
    { 5, std::string("IQX "  ) },
    { 7, std::string("IQX "  ) },
    { 8, std::string("IQX "  ) },
    { 6, std::string("ABCXZ ") }
};

/// Initialize the static frequency map for SatSys::sbas. Values are
/// {band, frequency in MHz}.
const std::map<int, double>
ngpt::satellite_system_traits<ngpt::SatSys::sbas>::frequency_map =
{
    { 1, 1575.42e0 },
    { 5, 1176.45e0 }
};

/// Initialize the valid-attribute-list Vs band map for SatSys::sbas.
const std::map<int, std::string>
ngpt::satellite_system_traits<ngpt::SatSys::sbas>::valid_attributes =
{
    { 1, std::string("C "  ) },
    { 5, std::string("IQX ") }
};

/// Initialize the static frequency map for SatSys::qzss. Values are
/// {band, frequency in MHz}.
const std::map<int, double>
ngpt::satellite_system_traits<ngpt::SatSys::qzss>::frequency_map =
{
    { 1, 1575.42e0 },
    { 2, 1227.60e0 },
    { 5, 1176.45e0 },
    { 6, 1278.75e0 } ///< LEX
};

/// Initialize the valid-attribute-list Vs band map for SatSys::qzss.
/// @note qszz observations are only recordable in RINEX v3.00 and onwards. Hence,
///       such an observable must have an attribute (i.e. ' ' is not a valid
///       attribute for qzss).
const std::map<int, std::string>
ngpt::satellite_system_traits<ngpt::SatSys::qzss>::valid_attributes =
{
    { 1, std::string("CSLXZ") },
    { 2, std::string("SLX"  ) },
    { 5, std::string("IQX"  ) },
    { 6, std::string("SLX"  ) }
};

/// Initialize the static frequency map for SatSys::beidou. Values are
/// {band, frequency in MHz}.
const std::map<int, double>
ngpt::satellite_system_traits<ngpt::SatSys::beidou>::frequency_map =
{
    { 1, 1561.098e0 },
    { 2, 1207.140e0 },
    { 3, 1268.520e0 }
};

/// Initialize the valid-attribute-list Vs band map for SatSys::beidou.
/// @note BeiDou observations are only recordable in RINEX v3.00 and onwards. Hence,
///       such an observable must have an attribute (i.e. ' ' is not a valid
///       attribute for BeiDou).
const std::map<int, std::string>
ngpt::satellite_system_traits<ngpt::SatSys::beidou>::valid_attributes =
{
    { 1, std::string("IQX") },
    { 2, std::string("IQX") },
    { 3, std::string("IQX") }
};

/// Initialize the static frequency map for SatSys::irnss. Values are
/// {band, frequency in MHz}.
/// @todo in rnx303 the 2nd frequency band is denoted as 'S'
const std::map<int, double>
ngpt::satellite_system_traits<ngpt::SatSys::irnss>::frequency_map =
{
    { 5, 1176.450e0 },
    { 9, 2492.028e0 }
};

/// Initialize the valid-attribute-list Vs band map for SatSys::irnss.
/// @note Irnss observations are only recordable in RINEX v3.00 and onwards. Hence,
///       such an observable must have an attribute (i.e. ' ' is not a valid
///       attribute for IRNSS).
const std::map<int, std::string>
ngpt::satellite_system_traits<ngpt::SatSys::irnss>::valid_attributes =
{
    { 5, std::string("ABCX") },
    { 9, std::string("ABCX") }
};
