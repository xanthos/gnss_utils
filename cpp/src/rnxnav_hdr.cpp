#include "rnxnav.hpp"

/// No header line can have more than 80 chars. However, there are cases when
/// they  exceed this limit, just a bit ...
constexpr int MAX_HEADER_CHARS { 85 };

/// Max header lines.
constexpr int MAX_HEADER_LINES { 100 };

/// Read the first line of the Navigation RINEX file and resolve the version
/// and the satellite system. The RINEX file can be of any format (2.xx, 3.xx).
/// If it is a 3.xx, then it should also contain the 'N' identifier at the 20th
/// column. After reading the line, the stream will be left as is, at the
/// position it is (i.e. after the first line). The stream (aka __istream)
/// should be already open, else the function will fail.
///
/// @parameter[in] s At output, it will be the satellite system (ngpt::SatSys)
///                  of the navigation file, as resolved in the first line.
/// @return          The version of the navigation file as a float.
float
ngpt::NavRinex::read_version(ngpt::SatSys& s)
{
    char line[MAX_HEADER_CHARS];
    float version;

    // The stream should be open by now!
    assert(this->__istream.is_open());

    // Go to the top of the file.
    __istream.seekg(0);

    // Read the first line. Get version and system.
    // ----------------------------------------------------
    __istream.getline(line, MAX_HEADER_CHARS);
    // strtod will keep on reading untill a non-valid
    // char is read. Fuck this, lets split the string
    // so that it only reads the valid chars (for version).
    *(line+10) = '\0';
    version = std::strtod(line, nullptr);
    if (version > 3e0) {
        assert(line[20] == 'N');
        s = ngpt::char2satsys(line[40]);
    } else {
        switch (line[20]) {
            case 'N':
                s = ngpt::SatSys::gps;
                break;
            case 'G':
                s = ngpt::SatSys::glonass;
                break;
            default:
                s = ngpt::char2satsys(line[20]);
        }
    }

    return version;
}

int
ngpt::NavRinex::read_header()
{
    char line[MAX_HEADER_CHARS];
    char* end;

    // The stream should be open by now!
    assert(this->__istream.is_open());

    // Go to the top of the file and read the first line.
    // Get version and system.
    // ----------------------------------------------------
    __version = this->read_version(__satsys);

    // Read the second line. Must be 'PGM / RUN BY / DATE'
    // ----------------------------------------------------
    __istream.getline(line, MAX_HEADER_CHARS);
    assert (!strncmp(line+60, "PGM / RUN BY / DATE", 19));

    // Keep on readling lines until 'END OF HEADER'.
    // ----------------------------------------------------
    int dummy_it = 0;
    while (dummy_it < MAX_HEADER_LINES && 
            strncmp(line+60, "END OF HEADER", 13) ) {

        // Read the line: 'CORR TO SYSTEM TIME'
        // ----------------------------------------
        // This is an optional line; it contains the fields:
        // __start_date, the time of reference for system time corr
        // __mTauC (aka -TauC), the correction to system time scale (sec) to
        //   correct GLONASS system time to UTC(SU)
        // This field is only available in RInex V2.11
        if (!strncmp(line, "CORR TO SYSTEM TIME", 19)) {
            ngpt::year year(std::strtol(line, &end, 10));
            ngpt::month month(std::strtol(line+6, &end, 10));
            ngpt::day_of_month day(std::strtol(line+12, &end, 10));
            this->__start_date = ngpt::datetime<ngpt::seconds>(year, month, day);
            this->__mTauC = std::strtod(line+18, &end);
        }
        
        // one more line read ...
        dummy_it++;

        // get new line
        __istream.getline(line, MAX_HEADER_CHARS);
    }
    if (dummy_it >= MAX_HEADER_LINES) {
        throw std::runtime_error
        ("[ERROR] NavRinex::read_header -> Could not find 'END OF HEADER'.");
    }

    // set the input buffer to this position (aka exactly after END OF HEADER)
    this->__eoh = __istream.tellg();
    return 0;
}
