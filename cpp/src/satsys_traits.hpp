///
/// @file satsys_traits.hpp
///

#ifndef __SATELLITE_SYSTEM_TRAITS_HPP__
#define __SATELLITE_SYSTEM_TRAITS_HPP__

#include <map>

namespace ngpt
{

/// @enum SatSys
///
/// Enumeration for known satellite systems. This is extracted from rnx303
/// @note Each of the enumerations recorded here, should have a specialized
///       ngpt::satellite_system_traits<> template.
enum class SatSys
: char
{
    gps,      //< G
    glonass,  //< R
    sbas,     //< S
    galileo,  //< E
    beidou,   //< C
    qzss,     //< J
    irnss,    //< I
    mixed     //< M
};

/// @class satellite_system_traits
///
/// Traits for satellite systems. A collection of satellite system - specific
/// information for each system in ngpt::SatSys.
///
/// @tparam S Any SatSys enum.
/// @warning This is just an empty template class; it **should** be specialized
///          for every enum type in SatSys.
template<SatSys S>
    struct satellite_system_traits
{};

/// Specialize traits for Satellite System Gps
template<>
    struct satellite_system_traits<SatSys::gps>
{
    /// Identifier
    static constexpr char identifier { 'G' };
    
    /// Dictionary holding pairs of <frequency band, freq. value>.
    static const std::map<int, double> frequency_map;
    
    /// Dictionary holding pairs of <frequency band, std::string>. The
    /// string is a seqeuence of (the **only**) valid attributes for
    /// each frequency band.
    /// @bug This will allow the attribute 'N' (codeless) for L1 and L2 pseudo-
    ///      range observations, which is actually **NOT** allowed (see 
    ///      rnx303).
    static const std::map<int, std::string> valid_attributes;
};

/// Specialize traits for Satellite System Glonass
template<>
    struct satellite_system_traits<SatSys::glonass>
{
    /// Identifier
    static constexpr char identifier { 'R' };
    
    /// Dictionary holding pairs of <frequency band, freq. value>.
    static const std::map<int, double> frequency_map;
    
    /// Dictionary holding pairs of <frequency band, std::string>. The
    /// string is a seqeuence of (the **only**) valid attributes for
    /// each frequency band.
    static const std::map<int, std::string> valid_attributes;
};

/// Specialize traits for Satellite System Galileo
template<>
    struct satellite_system_traits<SatSys::galileo>
{
    /// Identifier
    static constexpr char identifier { 'E' };
    
    /// Dictionary holding pairs of <frequency band, freq. value>.
    static const std::map<int, double> frequency_map;
    
    /// Dictionary holding pairs of <frequency band, std::string>. The
    /// string is a seqeuence of (the **only**) valid attributes for
    /// each frequency band.
    static const std::map<int, std::string> valid_attributes;
};

/// Specialize traits for Satellite System SBAS
template<>
    struct satellite_system_traits<SatSys::sbas>
{
    /// Identifier
    static constexpr char identifier { 'S' };
    
    /// Dictionary holding pairs of <frequency band, freq. value>.
    static const std::map<int, double> frequency_map;
    
    /// Dictionary holding pairs of <frequency band, std::string>. The
    /// string is a seqeuence of (the **only**) valid attributes for
    /// each frequency band.
    static const std::map<int, std::string> valid_attributes;
};

/// Specialize traits for Satellite System BeiDou (BDS)
template<>
    struct satellite_system_traits<SatSys::beidou>
{
    /// Identifier
    static constexpr char identifier { 'C' };
    
    /// Dictionary holding pairs of <frequency band, freq. value>.
    static const std::map<int, double> frequency_map;
    
    /// Dictionary holding pairs of <frequency band, std::string>. The
    /// string is a seqeuence of (the **only**) valid attributes for
    /// each frequency band.
    static const std::map<int, std::string> valid_attributes;
};

/// Specialize traits for Satellite System QZSS
template<>
    struct satellite_system_traits<SatSys::qzss>
{
    /// Identifier
    static constexpr char identifier { 'J' };
    
    /// Dictionary holding pairs of <frequency band, freq. value>.
    static const std::map<int, double> frequency_map;
    
    /// Dictionary holding pairs of <frequency band, std::string>. The
    /// string is a seqeuence of (the **only**) valid attributes for
    /// each frequency band.
    static const std::map<int, std::string> valid_attributes;
};

/// Specialize traits for Satellite System IRNSS
template<>
    struct satellite_system_traits<SatSys::irnss>
{
    /// Identifier
    static constexpr char identifier { 'I' };
    
    /// Dictionary holding pairs of <frequency band, freq. value>.
    static const std::map<int, double> frequency_map;
    
    /// Dictionary holding pairs of <frequency band, std::string>. The
    /// string is a seqeuence of (the **only**) valid attributes for
    /// each frequency band.
    static const std::map<int, std::string> valid_attributes;
};

/// Specialize traits for Satellite System MIXED
template<>
    struct satellite_system_traits<SatSys::mixed>
{
    /// Identifier
    static constexpr char identifier { 'M' };
    
    /// Dictionary holding pairs of <frequency band, freq. value>.
    static const std::map<int, double> frequency_map;
    
    /// Dictionary holding pairs of <frequency band, std::string>. The
    /// string is a seqeuence of (the **only**) valid attributes for
    /// each frequency band.
    static const std::map<int, std::string> valid_attributes;
};

}//namespace ngpt
#endif
