///
/// @file obscode.cpp
/// @brief Definitions for obscode.hpp
///

#include "obscode.hpp"

/// The observation code (as string) must have at least 3 chars, denoting the
/// observation type (e.g. 'C', or 'L', or ....), the band number (i.e. an
/// int), and a third character denoting the attribute (as described in @cite rnx303)
/// Any leading whitespaces will be skipped. Any trailing characters after the
/// attribute will be ignored. E.g. all of the strings "C1C", "   C1C" and
/// "  C1CFOOBAR" are valid and will return the same code (i.e. C1C).
/// All valid 3-char observation types, are listed in @cite rnx303, at Tables 4 to 10,
/// for all possible satellite systems.
///
/// @param[in] str  A valid @cite rnx303 observation type string (i.e. including
///                 observation type, band and attribute)
/// @param[in] s    The satellite system the observation type belongs to.
/// @return         An ngpt::ObservationCode (not guranted to be valid).
///
/// @throw std::runtime_error The function will throw the string passed
///                 in is not valid (according to @cite rnx303).
///
/// @note  A valid input string does **NOT** guarantee that the observation type
///        type resolved will be valid (e.g. the function will not check if the
///        band number actually belongs to the satellite sys passed in). To 
///        perform a strict validation, use the ngpt::ObservationCode::validate
///        method on the resulting instance.
ngpt::ObservationCode
ngpt::resolveRnx303str(std::string str, ngpt::SatelliteSystem s)
{
    using ngpt::ObservationCode;
    using ngpt::SatelliteSystem;
    using ngpt::ObsType;
    using ngpt::ObsAttrib;
    
    SatelliteSystem _satsys {s};
    ObsType _type;
    int _band;
    ObsAttrib _attrib;

    int do_throw = 0;
    std::string::size_type i = 0;
    
    // str must hold at least three chars.
    if (str.size() >= 3) {
        // skip leading whitespaces
        while (str[i] == ' ' && i < str.size()) i++;
        if (str.size()-i >= 3) {
            switch (str[i]) { /* resolve observation type */
                case 'C':
                    _type = ObsType::pseudorange;
                    break;
                case 'L':
                    _type = ObsType::carrier_phase;
                    break;
                case 'D':
                    _type = ObsType::doppler;
                    break;
                case 'S':
                    _type = ObsType::signal_strength;
                    break;
                case 'I':
                    _type = ObsType::ionosphere_phase_delay;
                    break;
                case 'X':
                    _type = ObsType::receiver_channel_number;
                    break;
                default:
                    throw std::runtime_error("resolveRnx303str() :: invalid observation type.");
            }
            _band   = str[i+1] - '0'; /* resolve band */
            _attrib = str[i+2];
        } else {
            do_throw = 1;
        }
    } else {
        do_throw = 2;
    }

    if (do_throw) {
        std::string w("Invalid observation type \"" + str.substr(i) + "\"");
        throw std::runtime_error("resolveRnx303str() :: " + w);
    }

    return ObservationCode{_type, _band, _attrib, _satsys.satsys()};
}

/// The observation code (as string) must have at least 2 chars, denoting the
/// observation type (e.g. 'C', or 'L', or ....) and the band number (i.e. an
/// int). Note that in RINEX versions < 3, no attribute is present. This 
/// two-character observation codes do not easily allow a further
/// refinement of the code to account for the different possibilities how to
/// generate a specific observable, e.g., with respect to the underlying
/// code (P,Y,M code in GPS) or the channels (I,Q, A,B,C in Galileo, I,Q in the
/// new GPS L5 frequency, GPS L2C).
/// There is a problem here! For ngpt::SatSys::gps and ngpt::SatSys::glonass,
/// we cannot seperate between the ranging codes 'C1' and 'P1' and 'C2' and 'P2'
/// (they both are of type ngpt::ObservationCode::pseudorange and band 1 or 2,
/// hence e.g. 'C1' and 'P1' would be stored as the same ngpt::ObservationCode
/// since we have no attribute to reference). Hence the function will assign the
/// following attributes:
/// 'C1' -> 'C1C'
/// 'C2' -> 'C2C'
/// 'P1' -> 'C1P'
/// 'P2' -> 'C2P'
/// Note that for @cite rnx211 the only possible satellite systems, are
/// gps, glonass, sbas and galileo.
/// Any leading whitespaces will be skipped. Any trailing characters after the
/// attribute will be ignored. E.g. all of the strings "C1", "   C1" and
/// "  C1CFOOBAR" are valid and will return the same code (i.e. C1).
/// All valid 2-char observation types, are listed in @cite rnx211, at Sec. 10.1.1,
/// for all possible satellite systems.
///
/// @param[in] str  A valid @cite rnx211 observation type string (i.e. including
///                 observation type and band)
/// @param[in] s    The satellite system the observation type belongs to.
/// @return         An ngpt::ObservationCode (not guranted to be valid).
///
/// @throw std::runtime_error The function will throw the string passed
///                 in is not valid (according to @cite rnx211).
///
/// @note  A valid input string does **NOT** guarantee that the observation type
///        type resolved will be valid (e.g. the function will not check if the
///        band number actually belongs to the satellite sys passed in). To 
///        perform a strict validation, use the ngpt::ObservationCode::validate
///        method on the resulting instance.
ngpt::ObservationCode
ngpt::resolveRnx211str(std::string str, ngpt::SatelliteSystem s)
{
    using ngpt::ObservationCode;
    using ngpt::SatelliteSystem;
    using ngpt::ObsType;
    using ngpt::ObsAttrib;
    
    SatelliteSystem _satsys {s};
    ObsType _type;
    int _band;
    ObsAttrib _attrib;

    int do_throw = 0;
    std::string::size_type i = 0;
    
    // str must hold at least two chars.
    if (str.size() >= 2) {
        // skip leading whitespaces
        while (str[i] == ' ' && i < str.size()) i++;
        if (str.size()-i >= 2) {
            switch (str[i]) { /* resolve observation type */
                case 'C':
                    _type = ObsType::pseudorange;
                    _attrib = 'C';
                    break;
                case 'P':
                    _type = ObsType::pseudorange;
                    _attrib = 'P';
                    break;
                case 'L':
                    _type = ObsType::carrier_phase;
                    break;
                case 'D':
                    _type = ObsType::doppler;
                    break;
                case 'S':
                    _type = ObsType::signal_strength;
                    break;
                default:
                    throw std::runtime_error("resolveRnx211str() :: invalid observation type.");
            }
            _band   = str[i+1] - '0'; /* resolve band */
        } else {
            do_throw = 1;
        }
    } else {
        do_throw = 2;
    }

    if (do_throw) {
        std::string w("Invalid observation type \"" + str.substr(i) + "\"");
        throw std::runtime_error("resolveRnx211str() :: " + w);
    }

    return ObservationCode{_type, _band, _attrib, _satsys.satsys()};
}

/// Validate the ngpt::ObservationCode (calling instance), comparing against the
/// codes given in rnx303.
///
/// @return True if the instance is valid; false otherwise.
bool
ngpt::ObservationCode::validate() const
{
    using ngpt::SatSys;
    
    // validate that the band number is valid
    try {
        this->nominal_freq();
    } catch (std::runtime_error&) {
        return false;
    }

    // if ionosphere_phase_delay,then the attribute must be blank
    if (__type == ObsType::ionosphere_phase_delay)
        return __attrib == ' ';

    // if receiver_channel_number, then band must be 1 and attribute must
    // be blank
    if (__type == ObsType::receiver_channel_number)
        return (__attrib==' ' && __band==1);
    
    // validate attribute; first, get the correct valid_attributes list. then
    // check 
    std::string valattr;
    try {
        switch (__satsys.satsys()) {
            case SatSys::gps:
                valattr = ngpt::satellite_system_traits<SatSys::gps>::valid_attributes.at(__band);
                break;
            case SatSys::glonass:
                valattr = ngpt::satellite_system_traits<SatSys::glonass>::valid_attributes.at(__band);
                break;
            case SatSys::sbas:
                valattr = ngpt::satellite_system_traits<SatSys::sbas>::valid_attributes.at(__band);
                break;
            case SatSys::galileo:
                valattr = ngpt::satellite_system_traits<SatSys::galileo>::valid_attributes.at(__band);
                break;
            case SatSys::beidou:
                valattr = ngpt::satellite_system_traits<SatSys::beidou>::valid_attributes.at(__band);
                break;
            case SatSys::qzss:
                valattr = ngpt::satellite_system_traits<SatSys::qzss>::valid_attributes.at(__band);
                break;
            case SatSys::irnss:
                valattr = ngpt::satellite_system_traits<SatSys::irnss>::valid_attributes.at(__band);
                break;
            default:
                // std::map.at() will throw an std::out_of_range; let's do the
                // same so we won;t have to catch more than one exception types.
                throw std::out_of_range("");
        }
    } catch (std::out_of_range& e) {
        return false;
    }
    
    // find attribute (char) in valid chars list
    if (valattr.find(__attrib.__c) == std::string::npos) return false;

    // One last thing! C1N and C2N are invalid types (codeless), but 'N' is a
    // valid attribute for C1 and C2. Hence, we have to check that this is not
    // the case
    if (__satsys.satsys() == SatSys::gps && (__band==1 || __band==2)) {
        if (__type == ObsType::pseudorange && __attrib == 'N') {
            return false;
        }
    }

    return true;
}
