///
/// @file obscode.hpp
///

#ifndef __OBSERVATION_CODE_HPP__
#define __OBSERVATION_CODE_HPP__

#include "satsys.hpp"

namespace ngpt
{
/// @enum ObsType
///
/// Enumeration (strongly typed) for known Observation Types. This is 
/// extracted from @cite rnx303
enum class ObsType
: char 
{
    pseudorange,            ///< PseudoRange (in meters)
    carrier_phase,          ///< Carrier Phase (in whole cycles)
    doppler,                ///< Doppler (doppler shift)
    signal_strength,        ///< Signal Strength
    ionosphere_phase_delay, ///< @cite rnx303 , sec. 5.12
    receiver_channel_number ///< @cite rnx303 , sec. 5.13
};

/// @class ObsAttrib
///
/// Struct to hold observable attributes as in @cite rnx303.
/// An ngpt::ObsAttrib is actually a wrapper around a char, designating the
/// attribute of the observation. This is only relevant for observation codes
/// after RINEX v3.
struct ObsAttrib
{
    /// Default constructor; constructs an (invalid) attribute with a whitespace
    /// char
    /// @param[in] c A char designating the attribute. This will be stored
    ///              as (the same) char in the instance's member variable.
    ///              No check is performed at construction level; users can
    ///              use whatever character.
    explicit
    ObsAttrib(char c = ' ') noexcept
    : __c(c)
    {}

    /// Assignment from char.
    /// @param[in] c The attribute as character.
    ObsAttrib&
    operator=(char c) noexcept
    {
        this->__c = c;
        return *this;
    }

    /// Equality operator; just compare the underlying characters.
    /// @param[in] a An ngpt::ObsAttrib to compare against
    /// @return True if the uderlying char members are the same; false otherwise
    bool
    operator==(ObsAttrib a) const noexcept
    { return (__c == a.__c); }
    
    /// Equality operator; just compare the underlying char with another char
    /// @param[in] c A char to compare against
    /// @note   The function will only compare the passed-in character with the
    ///         instance's member.
    /// @return True if the uderlying char member is the same with the char
    ///         passed in; false otherwise
    bool
    operator==(char c) const noexcept
    { return (__c == c); }
    
    /// Equality operator; just compare the underlying char with another char
    /// @param[in] c A char to compare against
    /// @note   The function will only compare the passed-in character with the
    ///         instance's member.
    /// @return True if the uderlying char member is not the same with the char
    ///         passed in; false otherwise
    bool
    operator!=(char c) const noexcept
    { return !(this->operator==(c)); }

    /// Conversion to char
    /*
    explicit
    operator char() const { return __c; }
    */

    /// A char as defined in @cite rnx303
    char __c;
};


/// @class ObservationCode
///
/// This class holds an Observation Code, as described in @cite rnx303, that is a
/// collection of three parts 'tna' describing:
///     - t -> the observation type (aka ngpt::ObsType)
///     - n -> the band/frequency (aka an integer value denoting the band of
///            an ngpt::SatelliteSystem)
///     - a -> attribute, i.e. tracking mode or channel, e.g., I, Q, etc (aka
///            ngpt::ObsAttrib)
/// and of course the corresponding satellite system (aka ngpt::SatelliteSystem).
/// Rinex versions prior to v3., used a two-character descriptor for any
/// observation code; hence, there is an inheret ambiguity regarding the attribute
/// these codes belong to. If no attribute is available, the default one (i.e.
/// character ' ') will be used.
class ObservationCode
{
public:

    /// Null constructor; constructs an invalid observation code.
    ObservationCode()
    : __type{ObsType::pseudorange},
      __band{},
      __attrib{},
      __satsys{}
    {}

    /// Constructor
    ObservationCode(ObsType t, int b, ObsAttrib a, SatSys s)
    : __type{t},
      __band{b},
      __attrib{a},
      __satsys{s}
      {};

    /// Return nominal frequency in MHz
    /// @note For glonass, this is **NOT** the actual frequency of the carrier;
    ///       we need the SVN info to compute it.
    double
    nominal_freq() const
    { return __satsys.band2freq(__band); }

    /// Validate (according to @cite rnx303 )
    bool
    validate() const;

    /// Get the satellite system
    /// @return The instanece's satellite system as ngpt::SatelliteSystem
    SatelliteSystem
    satsys() const noexcept
    { return __satsys; }

private:
    ObsType          __type;   ///< the type as ngpt::ObsType
    int              __band;   ///< the frequency band as integer
    ObsAttrib        __attrib; ///< the attribute as ngpt::ObsAttrib
    SatelliteSystem  __satsys; ///< the satellite system as ngpt::SatelliteSystem
};//class ObservationCode

/// Resolve a @cite rnx303 string to an ngpt::ObservationCode instance.
ObservationCode
resolveRnx303str(std::string str, SatelliteSystem s=SatelliteSystem{});

/// Resolve a rnx211 string to an ngpt::ObservationCode instance.
ObservationCode
resolveRnx211str(std::string str, SatelliteSystem s=SatelliteSystem{});

}//namespace ngpt

#endif
