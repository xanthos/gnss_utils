///
/// @file satsys.hpp
///

#ifndef __SATELLITE_SYSTEM_HPP__
#define __SATELLITE_SYSTEM_HPP__

#include "satsys_traits.hpp"

namespace ngpt
{

/// Given a character, return the corresponding satellite system.
ngpt::SatSys
char2satsys(char c);

/// Given a satellite system, return its identifying character
char
satsys2char(ngpt::SatSys);

/// @class SatelliteSystem
///
/// Class to hold any (Global Navigation) Satellite System.
/// The basic (if not only) member variable is actually a ngpt::SatSys enumerator 
/// that identifies the Satellite System. The class is here to provide methods for
/// interacting with the/any Satellite System.
class SatelliteSystem
{
public:

    /// Default constructor; constructs a SatSys::gps instance.
    /// @param[in] s An ngpt::SatSys enumeration type.
    explicit
    SatelliteSystem(SatSys s=SatSys::gps) noexcept
    : __sys{s}
    {};

    /// @brief Constructor from a char
    /// @param[in] c An identifing character for any Satellite System.
    ///              The char must designate a Satellite System, see
    ///              ngpt::SatSys and ngpt::char2satsys .
    /// @throw Will throw if no matching satellite system is found (aka, if
    ///        ngpt::char2satsys throws.
    SatelliteSystem(char c)
    :__sys{char2satsys(c)}
    {};

    /// Return the identifying character of this SatelliteSystem (as 
    /// in @cite rnx303
    /// @return A char descibing the calling SatelliteSystem
    /// @throw Will throw if ngpt::satsys2char throws.
    char
    aschar() const
    { return satsys2char(__sys); }

    /// Equality operator.
    /// @param[in] s A ngpt::SatelliteSystem to compare against
    /// @return True if the underlying ngpt::SatSys are identical; false
    ///         otherwise.
    bool
    operator==(SatelliteSystem s) const noexcept
    { return __sys == s.__sys; }
    
    /// In-equality operator
    /// @param[in] s A ngpt::SatelliteSystem to compare against
    /// @return True if the underlying ngpt::SatSys are not identical; false
    ///         otherwise.
    bool
    operator!=(SatelliteSystem s) const noexcept
    { return !(*this == s); }

    /// Return nominal frequency in MHz for band
    double
    band2freq(int band) const;

    /// Get the ngpt::SatSys enumeration type
    /// @return The instance's ngpt::SatSys (enumeration) type
    SatSys
    satsys() const noexcept
    { return __sys; }

private:
    SatSys  __sys;  ///< The satellite sys. as ngpt::SatSys enumerator.
    
}; //class SatelliteSystem

} // namespace ngpt

#endif
