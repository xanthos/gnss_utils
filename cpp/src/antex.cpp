#include "antex.hpp"

using ngpt::Antex;

Antex::Antex(const char* filename)
    : __filename {filename},
      __istream  {filename, std::ios_base::in},
      __satsys   {ngpt::SatSys::mixed},
      __eoh      {0}
{
    // read header; if it fails throw.
    if (this->read_header()) {
        throw std::runtime_error("Antex::Antex() failed");
    }
}
