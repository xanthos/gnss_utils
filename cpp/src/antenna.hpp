///
/// @file antenna.hpp
///
/// @brief  GNSS Antenna (plus Radome) Class. Used both for stations and
///         satellites.

#ifndef __GNSS_ANTENNA__
#define __GNSS_ANTENNA__

namespace ngpt
{

    /// Namespace to hide antenna specific details.
    namespace antenna_details
    {
        /// Maximum number of characters describing a GNSS antenna model 
        /// (no radome).
        constexpr std::size_t antenna_model_max_chars { 15 };

        /// Maximum number of characters describing a GNSS antenna radome type.
        constexpr std::size_t antenna_radome_max_chars { 4 };

        /// Maximum number of characters describing a GNSS antenna serial number.
        constexpr std::size_t antenna_serial_max_chars { 20 };

        /// Maximum size to represent all fields, including whitespaces and
        /// the (last) null terminating character.
        constexpr std::size_t antenna_full_max_chars
        {  antenna_model_max_chars  + 1 /* whitespace */
         + antenna_radome_max_chars
         + antenna_serial_max_chars + 1 /* null-reminating char */
        };
    }

/*  @class    antenna
 * 
 *  @details  This class holds a GNSS Antenna either for a satellite or a 
 *            receiver. Every antenna is represented by a specific Antenna model
 *            name, a Radome model and a Serial Number. These are all concateneted
 *            in a char array (but not exactly a c-string). See the note below
 *            for how this character array is formed.
 * 
 *  @cite rcvr_ant
 * 
 *  @note     The c-string array allocated for each instance, looks like:
 *  @verbatim
  N = antenna_model_max_chars
  M = antenna_radome_max_chars
  K = antenna_serial_max_chars
  [0,     N)       antenna model name
  [N+1,   N+M)     antenna radome name
  [N+M+1, N+M+K+1) antenna serial number

  | model name      |   | radome      | serial number     
  v                 v   v             v    
  +---+---+-...-+---+---+---+-...-+---+-----+-----+-...-+-------+
  | 0 | 1 |     |N-1| N |N+1|     |N+M|N+M+1|N+M+2|     |N+M+K+1|
  +---+---+-...-+---+---+---+-...-+---+-----+-----+-...-+-------+
                      ^                                     ^       
                      |                                     |      
            whitespace character                           '\0'

  \endverbatim
 * 
 *  @example test_antenna.cpp
 *           An example to show how to use the Antenna class.
 */
class Antenna
{
public:
    /// Default constructor. Set all fields to whitespace except for the last
    /// one which will be set to '\0'
    Antenna() noexcept;
    
    /// Constructor from c-string.
    explicit
    Antenna (const char*) noexcept;

    /// Constructor from Antenna type plus Radome (if any).
    explicit
    Antenna (const std::string&) noexcept;

    /// Set the radome type to 'NONE'
    inline void
    set_none_radome() noexcept;
private:

    /// Fill all fields of the __name private member to the given character,
    /// except from the last char (aka antenna_full_max_chars-1) which will
    /// be set to '\0'
    /// @param[in] c A character to fill the c-string with
    inline void
    fill_with(char c) noexcept;
    
    /// Copy antenna/radome from an std::string (to __name).
    void
    copy_from_str(const std::string&) noexcept;

    /// Copy antenna/radome pair from a c-string. 
    void
    copy_from_cstr(const char*) noexcept;

    /// Combined antenna, radome and serian number.
    char __name[antenna_details::antenna_full_max_chars]; 

}; // end Antenna

} // end ngpt

#endif
