#include <iostream>
#include <cassert>
#include <vector>
#include "obscode.hpp"

int main()
{
    using ngpt::ObservationCode;
    using ngpt::SatelliteSystem;

    std::cout<<"\n-----------------------------------------------------------";
    std::cout<<"\nTesting Observation Types (v211)";
    std::cout<<"\n-----------------------------------------------------------";

    /*
     * Invalid observation types, can throw at construction time.
     * They must always throw if the user invokes the nominal_freq() function.
     * They must always return false if the user invokes the validate() function.
     */

    ObservationCode o;

    // Rinex v303, GPS band 1
    std::vector<std::string> gps1 {" C1", "P1", "  L1", "D1", "S1"};
    for(auto git = gps1.begin(); git != gps1.end(); ++git) {
        // Note that following the three chars, we can have anything
        o = resolveRnx211str(*git+"foobar", SatelliteSystem{ngpt::SatSys::gps});
        assert( o.nominal_freq() == 1575.42e0 );
    }
    // Rinex v303, GPS band 2
    std::vector<std::string> gps2 {" C2", "P2", "  L2", "D2", "S2"};
    for(auto git = gps2.begin(); git != gps2.end(); ++git) {
        // Note that following the three chars, we can have anything
        o = resolveRnx211str(*git, SatelliteSystem{ngpt::SatSys::gps});
        assert( o.nominal_freq() == 1227.60e0 );
    }
    // Rinex v303, GPS band 5
    std::vector<std::string> gps5 {" C5", "  L5", "D5", "S5"};
    for(auto git = gps5.begin(); git != gps5.end(); ++git) {
        // Note that following the three chars, we can have anything
        o = resolveRnx211str(*git, SatelliteSystem{ngpt::SatSys::gps});
        assert( o.nominal_freq() == 1176.45e0 );
    }
    // Test some invalid codes
    std::vector<std::string> gpsin {"N1N", "V2", "B4A", "L333V", "L9C"};
    for(auto git = gpsin.begin(); git != gpsin.end(); ++git) {
        try {
            o = resolveRnx211str(*git, SatelliteSystem{ngpt::SatSys::gps});
            if (o.validate()) {
                std::cout<<"\n[ERROR] Validated observable: "<<*git<<"; that's an error";
            } /*else {
                std::cout<<"\n\tObservable "<<*git<<" return false for validate(); that's good!";
            }*/
            assert( !(o.validate()) );
            try {
                o.nominal_freq();
            } catch (std::runtime_error& e) {
                /*std::cout<<"\n\tObservable "<<*git<<" threw at nominal_freq(); that's good!";*/
            }
        } catch (std::runtime_error& e) {
            /*std::cout<<"\n\tObservable "<<*git<<" threw at constructor; that's good!";*/
        }
    }
    // Rinex v303, GLONASS band 1
    std::vector<std::string> glo1 {" C1", "P1", "  L1", "D1", "S1"};
    for(auto git = glo1.begin(); git != glo1.end(); ++git) {
        // Note that following the three chars, we can have anything
        o = resolveRnx211str(*git+"foobar", SatelliteSystem{ngpt::SatSys::glonass});
        assert( o.nominal_freq() == 1602e0 );
    }
    // Rinex v303, GLONASS band 2
    std::vector<std::string> glo2 {" C2", "P2", "  L2", "D2", "S2"};
    for(auto git = glo2.begin(); git != glo2.end(); ++git) {
        // Note that following the three chars, we can have anything
        o = resolveRnx211str(*git, SatelliteSystem{ngpt::SatSys::glonass});
        assert( o.nominal_freq() == 1246e0 );
    }
    // Test some invalid codes
    std::vector<std::string> gloin {"N1N", "V2", "B4A", "L333V", "L9C"};
    for(auto git = gloin.begin(); git != gloin.end(); ++git) {
        try {
            o = resolveRnx211str(*git, SatelliteSystem{ngpt::SatSys::glonass});
            if (o.validate()) {
                std::cout<<"\n[ERROR] Validated observable: "<<*git<<"; that's an error";
            } /*else {
                std::cout<<"\n\tObservable "<<*git<<" return false for validate(); that's good!";
            }*/
            assert( !(o.validate()) );
            try {
                o.nominal_freq();
            } catch (std::runtime_error& e) {
                /*std::cout<<"\n\tObservable "<<*git<<" threw at nominal_freq(); that's good!";*/
            }
        } catch (std::runtime_error& e) {
            /*std::cout<<"\n\tObservable "<<*git<<" threw at constructor; that's good!";*/
        }
    }
    // Rinex v303, GALILEO band 1
    std::vector<std::string> gal1 {"C1", "L1", "D1", "S1"};
    for(auto git = gal1.begin(); git != gal1.end(); ++git) {
        // Note that following the three chars, we can have anything
        o = resolveRnx211str(*git+"foobar", SatelliteSystem{ngpt::SatSys::galileo});
        assert( o.nominal_freq() == 1575.42e0 );
    }
    // Rinex v303, GALILEO band 5
    std::vector<std::string> gal5 {"C5", "L5", "D5", "S5"};
    for(auto git = gal5.begin(); git != gal5.end(); ++git) {
        // Note that following the three chars, we can have anything
        o = resolveRnx211str(*git+"foobar", SatelliteSystem{ngpt::SatSys::galileo});
        assert( o.nominal_freq() == 1176.45e0 );
    }
    // Rinex v303, GALILEO band 7
    std::vector<std::string> gal7 {"C7", "L7", "D7", "S7"};
    for(auto git = gal7.begin(); git != gal7.end(); ++git) {
        // Note that following the three chars, we can have anything
        o = resolveRnx211str(*git+"foobar", SatelliteSystem{ngpt::SatSys::galileo});
        assert( o.nominal_freq() == 1207.14e0 );
    }
    // Rinex v303, GALILEO band 8
    std::vector<std::string> gal8 {"C8", "L8", "D8", "S8"};
    for(auto git = gal8.begin(); git != gal8.end(); ++git) {
        // Note that following the three chars, we can have anything
        o = resolveRnx211str(*git+"foobar", SatelliteSystem{ngpt::SatSys::galileo});
        assert( o.nominal_freq() == 1191.795e0 );
    }
    // Rinex v303, GALILEO band 6
    std::vector<std::string> gal6 {"C6", "L6", "D6", "S6"};
    for(auto git = gal6.begin(); git != gal6.end(); ++git) {
        // Note that following the three chars, we can have anything
        o = resolveRnx211str(*git+"foobar", SatelliteSystem{ngpt::SatSys::galileo});
        assert( o.nominal_freq() == 1278.75e0 );
    }
    // Test some invalid codes
    std::vector<std::string> galin {"C7A", "T8B", "C2R", "R1L", "V2", "B4A", "L9C"};
    for(auto git = galin.begin(); git != galin.end(); ++git) {
        try {
            o = resolveRnx211str(*git, SatelliteSystem{ngpt::SatSys::galileo});
            if (o.validate()) {
                std::cout<<"\n[ERROR] Validated observable: "<<*git<<"; that's an error";
            } else {
                // std::cout<<"\n\tObservable "<<*git<<" return false for validate(); that's good!";
            }
            assert( !(o.validate()) );
            try {
                o.nominal_freq();
            } catch (std::runtime_error& e) {
                // std::cout<<"\n\tObservable "<<*git<<" threw at nominal_freq(); that's good!";
            }
        } catch (std::runtime_error& e) {
            // std::cout<<"\n\tObservable "<<*git<<" threw at constructor; that's good!";
        }
    }
    // Rinex v303, SBAS band 1
    std::vector<std::string> sba1 {"C1", "L1", "D1", "S1"};
    for(auto git = sba1.begin(); git != sba1.end(); ++git) {
        // Note that following the three chars, we can have anything
        o = resolveRnx211str(*git+"foobar", SatelliteSystem{ngpt::SatSys::sbas});
        assert( o.nominal_freq() == 1575.42e0 );
    }
    // Rinex v303, SBAS band 5
    std::vector<std::string> sba5 {"C5", "L5", "D5", "S5"};
    for(auto git = sba5.begin(); git != sba5.end(); ++git) {
        // Note that following the three chars, we can have anything
        o = resolveRnx211str(*git+"foobar", SatelliteSystem{ngpt::SatSys::sbas});
        assert( o.nominal_freq() == 1176.45e0 );
    }
    // Test some invalid codes
    std::vector<std::string> sbain {"C7A", "T8B", "C2R", "R1L", "V2", "B4A", "L9C"};
    for(auto git = sbain.begin(); git != sbain.end(); ++git) {
        try {
            o = resolveRnx211str(*git, SatelliteSystem{ngpt::SatSys::sbas});
            if (o.validate()) {
                std::cout<<"\n[ERROR] Validated observable: "<<*git<<"; that's an error";
            } else {
                // std::cout<<"\n\tObservable "<<*git<<" return false for validate(); that's good!";
            }
            assert( !(o.validate()) );
            try {
                o.nominal_freq();
            } catch (std::runtime_error& e) {
                // std::cout<<"\n\tObservable "<<*git<<" threw at nominal_freq(); that's good!";
            }
        } catch (std::runtime_error& e) {
            // std::cout<<"\n\tObservable "<<*git<<" threw at constructor; that's good!";
        }
    }

    std::cout<<"\nAll tests OK!\n";
    return 0;
}
