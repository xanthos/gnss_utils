#include <iostream>
#include <cassert>
#include "satsys.hpp"

int main()
{
    std::cout<<"\n-----------------------------------------------------------";
    std::cout<<"\nTesting SatSys, SatelliteSystem";
    std::cout<<"\n-----------------------------------------------------------";

    bool did_throw = false;

    // Constructors
    ngpt::SatelliteSystem sg {ngpt::SatSys::gps};
    ngpt::SatelliteSystem sr {ngpt::SatSys::glonass};
    ngpt::SatelliteSystem ss {'S'};
    ngpt::SatelliteSystem se {'E'};
    ngpt::SatelliteSystem sc {'C'};
    ngpt::SatelliteSystem sj {ngpt::SatSys::qzss};
    ngpt::SatelliteSystem si {ngpt::SatSys::irnss};
    ngpt::SatelliteSystem sm {'M'};
    try {
        ngpt::SatelliteSystem sf {'F'};
    } catch (std::runtime_error& e) {
        did_throw = true;
    }
    assert( did_throw );

    // to char conversions
    assert( sg.aschar() == 'G' );
    assert( sr.aschar() == 'R' );
    assert( ss.aschar() == 'S' );
    assert( se.aschar() == 'E' );
    assert( sc.aschar() == 'C' );
    assert( sj.aschar() == 'J' );
    assert( si.aschar() == 'I' );
    assert( sm.aschar() == 'M' );

    // equality operators
    assert( sg != sr );
    assert( sg != sm );
    assert( sg == ngpt::SatelliteSystem{'G'} );
    assert( sr == ngpt::SatelliteSystem{'R'} );
    assert( sm == ngpt::SatelliteSystem{ngpt::SatSys::mixed} );

    std::cout<<"\nAll tests OK!\n";
    return 0;
}
