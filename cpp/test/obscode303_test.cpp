#include <iostream>
#include <cassert>
#include <vector>
#include "obscode.hpp"

int main()
{
    using ngpt::ObservationCode;
    using ngpt::SatelliteSystem;

    std::cout<<"\n-----------------------------------------------------------";
    std::cout<<"\nTesting Observation Types (v303)";
    std::cout<<"\n-----------------------------------------------------------";

    /*
     * Invalid observation types, can throw at construction time.
     * They must always throw if the user invokes the nominal_freq() function.
     * They must always return false if the user invokes the validate() function.
     */

     ObservationCode o;

    // Rinex v303, GPS band 1
    std::vector<std::string> gps1 {" C1", "  L1", "D1", "S1"};
    for(auto git = gps1.begin(); git != gps1.end(); ++git) {
        std::vector<std::string> gps1a = {"CSLXPWYMN"};
        for(auto it = gps1a.begin(); it != gps1a.end(); ++it) {
            // Note that following the three chars, we can have anything
            std::string obsstr = (*git) + (*it) + "FOOBAR";
            o = resolveRnx303str(obsstr, SatelliteSystem{ngpt::SatSys::gps});
            // C1N is NOT valid
            if ( !(o.validate()) ) {
                assert( *git == "C1" && *it == "N");
            } else {
                if (*git == "C1") assert (*it != "N");
            }
            assert( o.nominal_freq() == 1575.42e0 );
        }
    }
    // Rinex v303, GPS band 2
    std::vector<std::string> gps2 {"C2", "L2", "D2", "S2"};
    for(auto git = gps2.begin(); git != gps2.end(); ++git) {
        std::vector<std::string> gps2a = {"CDSLXPWYMN"};
        for(auto it = gps2a.begin(); it != gps2a.end(); ++it) {
            std::string obsstr = (*git) + (*it);
            o = resolveRnx303str(obsstr, SatelliteSystem{ngpt::SatSys::gps});
            // C1N is NOT valid
            if ( !(o.validate()) ) {
                assert( *git == "C2" && *it == "N");
            } else {
                if (*git == "C2") assert (*it != "N");
            }
            assert( o.nominal_freq() == 1227.60e0 );
        }
    }
    // Rinex v303, GPS band 5
    std::vector<std::string> gps5 {"C5", "L5", "D5", "S5"};
    for(auto git = gps5.begin(); git != gps5.end(); ++git) {
        std::vector<std::string> gps5a = {"IQX"};
        for(auto it = gps5a.begin(); it != gps5a.end(); ++it) {
            std::string obsstr = (*git) + (*it);
            o = resolveRnx303str(obsstr, SatelliteSystem{ngpt::SatSys::gps});
            assert( o.validate() );
            assert( o.nominal_freq() == 1176.45e0 );
        }
    }
    // Test some invalid codes
    std::vector<std::string> gpsin {"C1N", "V2", "B4A", "L1V", "L9C"};
    for(auto git = gpsin.begin(); git != gpsin.end(); ++git) {
        try {
            o = resolveRnx303str(*git, SatelliteSystem{ngpt::SatSys::gps});
            if (o.validate()) {
                std::cout<<"\n[ERROR] Validated observable: "<<*git<<"; that's an error";
            } /*else {
                std::cout<<"\n\tObservable "<<*git<<" return false for validate(); that's good!";
            }*/
            assert( !(o.validate()) );
            try {
                o.nominal_freq();
            } catch (std::runtime_error& e) {
                /*std::cout<<"\n\tObservable "<<*git<<" threw at nominal_freq(); that's good!";*/
            }
        } catch (std::runtime_error& e) {
            /*std::cout<<"\n\tObservable "<<*git<<" threw at constructor; that's good!";*/
        }
    }

    // Rinex v303, GLONASS band 1
    std::vector<std::string> glo1 {"C1", "L1", "D1", "S1"};
    for(auto git = glo1.begin(); git != glo1.end(); ++git) {
        std::vector<std::string> glo1a = {"CP"};
        for(auto it = glo1a.begin(); it != glo1a.end(); ++it) {
            std::string obsstr = (*git) + (*it);
            o = resolveRnx303str(obsstr, SatelliteSystem{ngpt::SatSys::glonass});
            assert( o.nominal_freq() == 1602e0 );
        }
    }
    // Rinex v303, GLONASS band 2
    std::vector<std::string> glo2 {"C2", "L2", "D2", "S2"};
    for(auto git = glo2.begin(); git != glo2.end(); ++git) {
        std::vector<std::string> glo2a = {"CP"};
        for(auto it = glo2a.begin(); it != glo2a.end(); ++it) {
            std::string obsstr = (*git) + (*it);
            o = resolveRnx303str(obsstr, SatelliteSystem{ngpt::SatSys::glonass});
            assert( o.nominal_freq() == 1246e0 );
        }
    }
    // Rinex v303, GLONASS band 3
    std::vector<std::string> glo3 {"C3", "L3", "D3", "S3"};
    for(auto git = glo3.begin(); git != glo3.end(); ++git) {
        std::vector<std::string> glo3a = {"IQX"};
        for(auto it = glo3a.begin(); it != glo3a.end(); ++it) {
            std::string obsstr = (*git) + (*it);
            o = resolveRnx303str(obsstr, SatelliteSystem{ngpt::SatSys::glonass});
            assert( o.validate() );
            assert( o.nominal_freq() == 1202.025e0 );
        }
    }
    // Test some invalid codes
    std::vector<std::string> gloin {"C1N", "C1L", "V2", "B4A", "L1V", "L9C"};
    for(auto git = gloin.begin(); git != gloin.end(); ++git) {
        try {
            o = resolveRnx303str(*git, SatelliteSystem{ngpt::SatSys::glonass});
            if (o.validate()) {
                std::cout<<"\n[ERROR] Validated observable: "<<*git<<"; that's an error";
            } else {
                // std::cout<<"\n\tObservable "<<*git<<" return false for validate(); that's good!";
            }
            assert( !(o.validate()) );
            try {
                o.nominal_freq();
            } catch (std::runtime_error& e) {
                // std::cout<<"\n\tObservable "<<*git<<" threw at nominal_freq(); that's good!";
            }
        } catch (std::runtime_error& e) {
            // std::cout<<"\n\tObservable "<<*git<<" threw at constructor; that's good!";
        }
    }

    // Rinex v303, GALILEO band 1
    std::vector<std::string> gal1 {"C1", "L1", "D1", "S1"};
    for(auto git = gal1.begin(); git != gal1.end(); ++git) {
        std::vector<std::string> gal1a = {"ABCXZ"};
        for(auto it = gal1a.begin(); it != gal1a.end(); ++it) {
            std::string obsstr = (*git) + (*it);
            o = resolveRnx303str(obsstr, SatelliteSystem{ngpt::SatSys::galileo});
            assert( o.nominal_freq() == 1575.42e0 );
        }
    }
    // Rinex v303, GALILEO band E5a (i.e. 5)
    std::vector<std::string> gal5 {"C5", "L5", "D5", "S5"};
    for(auto git = gal5.begin(); git != gal5.end(); ++git) {
        std::vector<std::string> gal5a = {"IQX"};
        for(auto it = gal5a.begin(); it != gal5a.end(); ++it) {
            std::string obsstr = (*git) + (*it);
            o = resolveRnx303str(obsstr, SatelliteSystem{ngpt::SatSys::galileo});
            assert( o.nominal_freq() == 1176.45e0 );
        }
    }
    // Rinex v303, GALILEO band E5b (i.e. 7)
    std::vector<std::string> gal7 {"C7", "L7", "D7", "S7"};
    for(auto git = gal7.begin(); git != gal7.end(); ++git) {
        std::vector<std::string> gal7a = {"IQX"};
        for(auto it = gal7a.begin(); it != gal7a.end(); ++it) {
            std::string obsstr = (*git) + (*it);
            o = resolveRnx303str(obsstr, SatelliteSystem{ngpt::SatSys::galileo});
            assert( o.nominal_freq() == 1207.140e0 );
        }
    }
    // Rinex v303, GALILEO band E5a+b (i.e. 8)
    std::vector<std::string> gal8 {"C8", "L8", "D8", "S8"};
    for(auto git = gal8.begin(); git != gal8.end(); ++git) {
        std::vector<std::string> gal8a = {"IQX"};
        for(auto it = gal8a.begin(); it != gal8a.end(); ++it) {
            std::string obsstr = (*git) + (*it);
            o = resolveRnx303str(obsstr, SatelliteSystem{ngpt::SatSys::galileo});
            assert( o.nominal_freq() == 1191.795e0 );
        }
    }
    // Rinex v303, GALILEO band 6
    std::vector<std::string> gal6 {"C6", "L6", "D6", "S6"};
    for(auto git = gal6.begin(); git != gal6.end(); ++git) {
        std::vector<std::string> gal6a = {"ABCXZ"};
        for(auto it = gal6a.begin(); it != gal6a.end(); ++it) {
            std::string obsstr = (*git) + (*it);
            o = resolveRnx303str(obsstr, SatelliteSystem{ngpt::SatSys::galileo});
            assert( o.nominal_freq() == 1278.75e0 );
        }
    }
    // Test some invalid codes
    std::vector<std::string> galin {"C7A", "L8B", "C1R", "R1L", "V2", "B4A", "L1V", "L9C"};
    for(auto git = galin.begin(); git != galin.end(); ++git) {
        try {
            o = resolveRnx303str(*git, SatelliteSystem{ngpt::SatSys::galileo});
            if (o.validate()) {
                std::cout<<"\n[ERROR] Validated observable: "<<*git<<"; that's an error";
            } else {
                // std::cout<<"\n\tObservable "<<*git<<" return false for validate(); that's good!";
            }
            assert( !(o.validate()) );
            try {
                o.nominal_freq();
            } catch (std::runtime_error& e) {
                // std::cout<<"\n\tObservable "<<*git<<" threw at nominal_freq(); that's good!";
            }
        } catch (std::runtime_error& e) {
            // std::cout<<"\n\tObservable "<<*git<<" threw at constructor; that's good!";
        }
    }
    // Rinex v303, SBAS band 1
    std::vector<std::string> sba1 {"C1", "L1", "D1", "S1"};
    for(auto git = sba1.begin(); git != sba1.end(); ++git) {
        std::vector<std::string> sba1a = {"C"};
        for(auto it = sba1a.begin(); it != sba1a.end(); ++it) {
            std::string obsstr = (*git) + (*it);
            o = resolveRnx303str(obsstr, SatelliteSystem{ngpt::SatSys::sbas});
            assert( o.nominal_freq() == 1575.42e0 );
        }
    }
    // Rinex v303, SBAS band 5
    std::vector<std::string> sba5 {"C5", "L5", "D5", "S5"};
    for(auto git = sba5.begin(); git != sba5.end(); ++git) {
        std::vector<std::string> sba5a = {"IQX"};
        for(auto it = sba5a.begin(); it != sba5a.end(); ++it) {
            std::string obsstr = (*git) + (*it);
            o = resolveRnx303str(obsstr, SatelliteSystem{ngpt::SatSys::sbas});
            assert( o.nominal_freq() == 1176.45e0 );
        }
    }
    // Test some invalid codes
    std::vector<std::string> sbain {"C1A", "L2C", "L2C", "R1L", "V2", "B4A", "L1V", "L9C"};
    for(auto git = sbain.begin(); git != sbain.end(); ++git) {
        try {
            o = resolveRnx303str(*git, SatelliteSystem{ngpt::SatSys::sbas});
            if (o.validate()) {
                std::cout<<"\n[ERROR] Validated observable: "<<*git<<"; that's an error";
            } else {
                // std::cout<<"\n\tObservable "<<*git<<" return false for validate(); that's good!";
            }
            assert( !(o.validate()) );
            try {
                o.nominal_freq();
            } catch (std::runtime_error& e) {
                // std::cout<<"\n\tObservable "<<*git<<" threw at nominal_freq(); that's good!";
            }
        } catch (std::runtime_error& e) {
            // std::cout<<"\n\tObservable "<<*git<<" threw at constructor; that's good!";
        }
    }
    // Rinex v303, QZSS band 1
    std::vector<std::string> qzs1 {"C1", "L1", "D1", "S1"};
    for(auto git = qzs1.begin(); git != qzs1.end(); ++git) {
        std::vector<std::string> qzs1a = {"CSLXZ"};
        for(auto it = qzs1a.begin(); it != qzs1a.end(); ++it) {
            std::string obsstr = (*git) + (*it);
            o = resolveRnx303str(obsstr, SatelliteSystem{ngpt::SatSys::qzss});
            assert( o.nominal_freq() == 1575.42e0 );
        }
    }
    // Rinex v303, QZSS band 2
    std::vector<std::string> qzs2 {"C2", "L2", "D2", "S2"};
    for(auto git = qzs2.begin(); git != qzs2.end(); ++git) {
        std::vector<std::string> qzs2a = {"SLX"};
        for(auto it = qzs2a.begin(); it != qzs2a.end(); ++it) {
            std::string obsstr = (*git) + (*it);
            o = resolveRnx303str(obsstr, SatelliteSystem{ngpt::SatSys::qzss});
            assert( o.nominal_freq() == 1227.60e0 );
        }
    }
    // Rinex v303, QZSS band 5
    std::vector<std::string> qzs5 {"C5", "L5", "D5", "S5"};
    for(auto git = qzs5.begin(); git != qzs5.end(); ++git) {
        std::vector<std::string> qzs5a = {"IQX"};
        for(auto it = qzs5a.begin(); it != qzs5a.end(); ++it) {
            std::string obsstr = (*git) + (*it);
            o = resolveRnx303str(obsstr, SatelliteSystem{ngpt::SatSys::qzss});
            assert( o.nominal_freq() == 1176.45e0 );
        }
    }
    // Rinex v303, QZSS band LEX(6)
    std::vector<std::string> qzs6 {"C6", "L6", "D6", "S6"};
    for(auto git = qzs6.begin(); git != qzs6.end(); ++git) {
        std::vector<std::string> qzs6a = {"SLX"};
        for(auto it = qzs6a.begin(); it != qzs6a.end(); ++it) {
            std::string obsstr = (*git) + (*it);
            o = resolveRnx303str(obsstr, SatelliteSystem{ngpt::SatSys::qzss});
            assert( o.nominal_freq() == 1278.75e0 );
        }
    }
    // Test some invalid codes
    std::vector<std::string> qzsin {"C1Q", "L1I", "D3C", "L2C", "C2Q", "C5A", "L6Q"};
    for(auto git = qzsin.begin(); git != qzsin.end(); ++git) {
        try {
            o = resolveRnx303str(*git, SatelliteSystem{ngpt::SatSys::qzss});
            if (o.validate()) {
                std::cout<<"\n[ERROR] Validated observable: "<<*git<<"; that's an error";
            } else {
                // std::cout<<"\n\tObservable "<<*git<<" return false for validate(); that's good!";
            }
            assert( !(o.validate()) );
            try {
                o.nominal_freq();
            } catch (std::runtime_error& e) {
                // std::cout<<"\n\tObservable "<<*git<<" threw at nominal_freq(); that's good!";
            }
        } catch (std::runtime_error& e) {
            // std::cout<<"\n\tObservable "<<*git<<" threw at constructor; that's good!";
        }
    }
    // Rinex v303, BeiDou band 1
    std::vector<std::string> bds1 {"C1", "L1", "D1", "S1"};
    for(auto git = bds1.begin(); git != bds1.end(); ++git) {
        std::vector<std::string> bds1a = {"IQX"};
        for(auto it = bds1a.begin(); it != bds1a.end(); ++it) {
            std::string obsstr = (*git) + (*it);
            o = resolveRnx303str(obsstr, SatelliteSystem{ngpt::SatSys::beidou});
            assert( o.nominal_freq() == 1561.098e0 );
        }
    }
    // Rinex v303, BeiDou band 2
    std::vector<std::string> bds2 {"C2", "L2", "D2", "S2"};
    for(auto git = bds2.begin(); git != bds2.end(); ++git) {
        std::vector<std::string> bds2a = {"IQX"};
        for(auto it = bds2a.begin(); it != bds2a.end(); ++it) {
            std::string obsstr = (*git) + (*it);
            o = resolveRnx303str(obsstr, SatelliteSystem{ngpt::SatSys::beidou});
            assert( o.nominal_freq() == 1207.14e0 );
        }
    }
    // Rinex v303, BeiDou band 3
    std::vector<std::string> bds3 {"C3", "L3", "D3", "S3"};
    for(auto git = bds3.begin(); git != bds3.end(); ++git) {
        std::vector<std::string> bds3a = {"IQX"};
        for(auto it = bds3a.begin(); it != bds3a.end(); ++it) {
            std::string obsstr = (*git) + (*it);
            o = resolveRnx303str(obsstr, SatelliteSystem{ngpt::SatSys::beidou});
            assert( o.nominal_freq() == 1268.52e0 );
        }
    }
    // Test some invalid codes
    std::vector<std::string> bdsin {"C1C", "L1P", "D3C", "L2C", "C2O", "C5A", "L6Q"};
    for(auto git = bdsin.begin(); git != bdsin.end(); ++git) {
        try {
            o = resolveRnx303str(*git, SatelliteSystem{ngpt::SatSys::beidou});
            if (o.validate()) {
                std::cout<<"\n[ERROR] Validated observable: "<<*git<<"; that's an error";
            } else {
                // std::cout<<"\n\tObservable "<<*git<<" return false for validate(); that's good!";
            }
            assert( !(o.validate()) );
            try {
                o.nominal_freq();
            } catch (std::runtime_error& e) {
                // std::cout<<"\n\tObservable "<<*git<<" threw at nominal_freq(); that's good!";
            }
        } catch (std::runtime_error& e) {
            // std::cout<<"\n\tObservable "<<*git<<" threw at constructor; that's good!";
        }
    }
    // Rinex v303, IRNSS band 5
    std::vector<std::string> irn5 {"C5", "L5", "D5", "S5"};
    for(auto git = irn5.begin(); git != irn5.end(); ++git) {
        std::vector<std::string> irn5a = {"ABCX"};
        for(auto it = irn5a.begin(); it != irn5a.end(); ++it) {
            std::string obsstr = (*git) + (*it);
            o = resolveRnx303str(obsstr, SatelliteSystem{ngpt::SatSys::irnss});
            assert( o.nominal_freq() == 1176.45e0 );
        }
    }
    // Rinex v303, IRNSS band S (band 9)
    std::vector<std::string> irn9 {"C9", "L9", "D9", "S9"};
    for(auto git = irn9.begin(); git != irn9.end(); ++git) {
        std::vector<std::string> irn9a = {"ABCX"};
        for(auto it = irn9a.begin(); it != irn9a.end(); ++it) {
            std::string obsstr = (*git) + (*it);
            o = resolveRnx303str(obsstr, SatelliteSystem{ngpt::SatSys::irnss});
            assert( o.nominal_freq() == 2492.028e0 );
        }
    }
    // Test some invalid codes
    std::vector<std::string> irnin {"C1C", "L1P", "D3C", "L2C", "C2O", "C5Q", "L6Q"};
    for(auto git = irnin.begin(); git != irnin.end(); ++git) {
        try {
            o = resolveRnx303str(*git, SatelliteSystem{ngpt::SatSys::irnss});
            if (o.validate()) {
                std::cout<<"\n[ERROR] Validated observable: "<<*git<<"; that's an error";
            } else {
                // std::cout<<"\n\tObservable "<<*git<<" return false for validate(); that's good!";
            }
            assert( !(o.validate()) );
            try {
                o.nominal_freq();
            } catch (std::runtime_error& e) {
                // std::cout<<"\n\tObservable "<<*git<<" threw at nominal_freq(); that's good!";
            }
        } catch (std::runtime_error& e) {
            // std::cout<<"\n\tObservable "<<*git<<" threw at constructor; that's good!";
        }
    }
    std::cout<<"\nAll tests OK!\n";
    return 0;
}
