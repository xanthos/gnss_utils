#include "rnxnav.hpp"

using namespace ngpt;

int main(int argc, char* argv[])
{
    if (argc < 2) {
        std::cerr<<"\n[ERROR] Need to provide Rinex v2.11 or lower navigation file\n";
        return 1;
    }

    NavRinex nav (argv[1]);

    ngpt::SatSys s;
    float v = nav.read_version(s);
    std::cout<<"\nNav Rinex version is: "<<v;
    std::cout<<"\nSatellite system is : "<<ngpt::satsys2char(s);

    std::cout<<"\n";
    return 0;
}
